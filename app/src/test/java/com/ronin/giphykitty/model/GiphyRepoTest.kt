package com.ronin.giphykitty.model

import com.ronin.giphykitty.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Spy
import org.mockito.junit.MockitoJUnit
import timber.log.Timber

class GiphyRepoTest {

    @Rule
    @JvmField
    var rule = MockitoJUnit.rule()

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    lateinit var mockApi: GiphyApi

    @Spy
    lateinit var spyDisposables: CompositeDisposable

    @Mock
    lateinit var mockTree: Timber.Tree

    private lateinit var mRepo: GiphyRepo

    @Before
    fun setup() {
        Timber.plant(mockTree)
        mRepo = GiphyRepo(mockApi, spyDisposables)
    }

    @After
    fun cleanup() {
        Timber.uprootAll()
    }

    @Test
    fun testSearch() {
        // onError
        val expectedThrowable = Throwable()
        `when`(mockApi.search(any(), any())).doReturn(Observable.error(expectedThrowable))
        mRepo.search("", {})

        // Test error was thrown and logged
        verify(mockTree).e(expectedThrowable)

        // onComplete
        val imagesObject = SearchResult.Images(
            SearchResult.FixedWidth("url")
        )
        val searchResult = SearchResult.Root(
            listOf(SearchResult.Gif(
                imagesObject
            ))
        )
        `when`(mockApi.search(any(), any())).thenReturn(Observable.just(searchResult))

        var callbackInvoked = false
        mRepo.search("", { callbackInvoked = true })

        // Test callback was called
        assertTrue(callbackInvoked)

        // Test image list was populated with expected value
        val expectedImagesList = listOf(imagesObject)
        assertEquals(expectedImagesList, mRepo.imagesList)
    }

    @Test
    fun testOnStop() {
        mRepo.onStop()
        verify(spyDisposables).clear()
    }

    @Test
    fun testOnDestroy() {
        mRepo.onDestroy()
        verify(spyDisposables).dispose()
    }
}