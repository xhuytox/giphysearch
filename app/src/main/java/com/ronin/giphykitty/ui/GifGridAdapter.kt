package com.ronin.giphykitty.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ronin.giphykitty.R
import com.ronin.giphykitty.model.SearchResult
import kotlinx.android.synthetic.main.layout_item_gif.view.*

class GifGridAdapter : RecyclerView.Adapter<GifGridAdapter.ViewHolder>() {

    class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        fun bindItem(item: SearchResult.Images) {
            Glide.with(mView.imgView.context)
                .asGif()
                .load(item.fixed_width.url)
                .placeholder(android.R.drawable.ic_menu_gallery)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mView.imgView)
        }
    }

    private var mData = listOf<SearchResult.Images>()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_gif, null)
        return ViewHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(mData[position])
    }

    fun setData(data: List<SearchResult.Images>) {
        mData = data
    }
}