package com.ronin.giphykitty.ui

import com.ronin.giphykitty.model.GiphyRepo
import javax.inject.Inject

class MainPresenter @Inject constructor (private val activity: MainActivity, private val repo: GiphyRepo) {

    fun search(searchString: String) {
        repo.search(searchString, ::refreshView)
    }

    private fun refreshView() {
        activity.refreshView(repo.imagesList)
    }

    fun onStop() {
        repo.onStop()
    }

    fun onDestroy() {
        repo.onDestroy()
    }
}