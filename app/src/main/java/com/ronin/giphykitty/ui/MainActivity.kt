package com.ronin.giphykitty.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.StaggeredGridLayoutManager
import android.widget.SearchView
import com.ronin.giphykitty.GiphySearchApplication
import com.ronin.giphykitty.R
import com.ronin.giphykitty.di.MainModule
import com.ronin.giphykitty.model.SearchResult
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var adapter: GifGridAdapter

    @Inject
    lateinit var viewManager: StaggeredGridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as GiphySearchApplication).appComponent
            .plus(MainModule(this))
            .inject(this)

        GifsRecyclerView.apply {
            layoutManager = viewManager
            adapter = this@MainActivity.adapter
            addItemDecoration(DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL))
        }
        searchSearchView.apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }

                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let {
                        presenter.search(it)
                        this@apply.clearFocus()
                        return true
                    }

                    return false
                }
            })
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    fun refreshView(imagesList: List<SearchResult.Images>) {
        adapter.setData(imagesList)
        adapter.notifyDataSetChanged()
    }
}
