package com.ronin.giphykitty.model

object SearchResult {
    data class Root(val data: List<Gif>)
    data class Gif(val images: Images)
    data class Images(val fixed_width: FixedWidth)
    data class FixedWidth(val url: String)
}