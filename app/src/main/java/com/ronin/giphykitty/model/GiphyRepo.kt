package com.ronin.giphykitty.model

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class GiphyRepo @Inject constructor (
    private val mApi: GiphyApi,
    private val mDisposables: CompositeDisposable
) {

    private val SEARCH_LIMIT = 50

    private val m_imagesList: ArrayList<SearchResult.Images> = ArrayList()
    val imagesList: List<SearchResult.Images>
        get() = m_imagesList.toList()

    fun search(searchString: String, callback: () -> Unit) {
        mDisposables.add(
            mApi.search(searchString, SEARCH_LIMIT)
                .subscribeOn(Schedulers.io())
                .map { result -> result.data }
                .flatMap { gifsList -> Observable.fromIterable(gifsList) }
                .map { gif -> gif.images }
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                    { value ->
                        m_imagesList.clear()
                        m_imagesList.addAll(value)
                        callback()
                    },
                    { error -> Timber.e(error) }
                )
        )
    }

    fun onStop() {
        mDisposables.clear()
    }

    fun onDestroy() {
        mDisposables.dispose()
    }
}