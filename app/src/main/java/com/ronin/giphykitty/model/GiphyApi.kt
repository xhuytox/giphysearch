package com.ronin.giphykitty.model

import com.ronin.giphykitty.BuildConfig
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface GiphyApi {

    @GET("/v1/gifs/search?api_key=" + BuildConfig.API_KEY)
    fun search(
        @Query("q") searchString: String,
        @Query("limit") limit: Int
    ): Observable<SearchResult.Root>
}