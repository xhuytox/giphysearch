package com.ronin.giphykitty.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun plus(module: MainModule): MainComponent
}