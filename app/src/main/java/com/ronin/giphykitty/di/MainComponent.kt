package com.ronin.giphykitty.di

import com.ronin.giphykitty.ui.MainActivity
import dagger.Subcomponent

@MainActivityScope
@Subcomponent(modules = [MainModule::class])
interface MainComponent {
    fun inject(mainActivity: MainActivity)
}