package com.ronin.giphykitty.di

import android.support.v7.widget.StaggeredGridLayoutManager
import com.ronin.giphykitty.model.GiphyApi
import com.ronin.giphykitty.model.GiphyRepo
import com.ronin.giphykitty.ui.GifGridAdapter
import com.ronin.giphykitty.ui.MainActivity
import com.ronin.giphykitty.ui.MainPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class MainModule(private val mainActivity: MainActivity) {

    @MainActivityScope
    @Provides
    fun provideMainPresenter(repo: GiphyRepo): MainPresenter {
        return MainPresenter(mainActivity, repo)
    }

    @MainActivityScope
    @Provides
    fun provideGiphyRepo(api: GiphyApi, disposable: CompositeDisposable): GiphyRepo {
        return GiphyRepo(api, disposable)
    }

    @MainActivityScope
    @Provides
    fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

    @MainActivityScope
    @Provides
    fun provideGifGridAdapter(): GifGridAdapter {
        return GifGridAdapter()
    }

    @MainActivityScope
    @Provides
    fun provideStaggeredGridLayoutManager(): StaggeredGridLayoutManager {
        return StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
    }
}