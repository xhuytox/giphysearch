package com.ronin.giphykitty

import android.app.Application
import com.ronin.giphykitty.di.AppComponent
import com.ronin.giphykitty.di.DaggerAppComponent
import timber.log.Timber

class GiphySearchApplication : Application() {

    lateinit var appComponent: AppComponent private set

    @Override
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        appComponent = DaggerAppComponent.builder().build()
    }
}